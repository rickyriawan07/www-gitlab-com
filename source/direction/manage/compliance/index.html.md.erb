---
layout: markdown_page
title: "Group Direction - Compliance"
canonical_path: "/direction/manage/compliance/"
---

This page is for the [Compliance group](https://about.gitlab.com/handbook/product/categories/#compliance-group). It describes high-level goals
and direction for our group. Check out the
[Manage section](https://about.gitlab.com/direction/manage) page to see what
the rest of our stage is working on, how we fit in, and our individual category pages
to get fine-grained details.

<%= partial("direction/manage/compliance/templates/overview") %>

## Compliance, Regulatory Requirements, and Security

Our compliance controls and reporting provide value to any organization, whether they must adhere to regulatory standards
or not. At GitLab, we view compliance as a way to help businesses define, implement, and manage policy-based controls for their
organization. This helps for meeting regulatory requirements and provides real value by helping organizations manage risk
inside their business.

As we develop compliance, our goal is to go well beyond "checking a box" needed for a standard, but to ensure
that our capabilities deliver value by protecting the business and reducing risk. We do this by thinking through each of 
our new capabilities and considering how a malicious actor may try to bypass, circumvent, or disable it.
We view compliance and security as very closely related and two areas that must work effectively together. Compliance defines workflow and policy requirements, enforces them, and monitors adherence to them. Security is all about ensuring what is being defined, enforced, or monitored cannot be bypassed, disabled, or modified inappropriately. Having compliance without security or security without compliance means an organization either is just checking a box with a false sense of security or is enforcing arbitrary restrictions, respectively. If we add something that meets a requirement for a standard, but does not prevent a malicious actor from negatively impacting the business, we're not done - we need to do more.

This approach gives GitLab a way to differentiate itself from other compliance tools on the market. Other tools are reactive and help identify blind spots or deviations after they have occurred, while GitLab's goal is to proactively prevent these problems from occurring in the first place. Organizations
may view compliance as unneeded overhead for reports, but at GitLab our goal is to ensure our users
are safe, the risk to the business is minimized, and that compliance helps the business move more quickly.

## Jobs to be Done

To best frame these problems we’ve compiled a set of Jobs To Be Done (JTBD) that represent the jobs our users are hiring us for. If you’re unfamiliar with JTBDs take a look at [this article](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done). 

<%= partial("direction/jtbd-list", locals: { stage_key: "Compliance" }) %>

### Personas we work with
 
[Cameron](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager), the Compliance Manager, is one of our
key personas we focus on. Cameron has many different jobs, such as those listed above, and we want to ensure they can be effective in doing them and making
those jobs easier.

We know that Compliance affects an entire organization when policies and business requirements
are added. While not our primary personas, we focus on how our capabilities impact others,
such as [Sasha](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer),
[Delaney](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead),
[Devon](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer).
Our goal is to ensure those other personas are minimally impacted and can do their jobs while still remaining
compliant with the organization's policies. Reducing the friction between the compliance needs of a business
and team's needs to get work done will be a key way GitLab can add value for our users.

## How we approach problems
The problems and use cases we focus on can be quite large, so it helps to break them down into smaller pieces. As a result we focus on discovering the most [minimal viable change](https://about.gitlab.com/handbook/values/#minimal-viable-change-mvc) possible that still solves a particular problem by trying ["boring solutions"](https://about.gitlab.com/handbook/values/#boring-solutions) first.

### Traceability, visibility, and actionability
Use cases we focus on can be broad and could go in many directions. We will focus on approaching these by starting from
traceability, moving to better visibility, and then providing actionability. What we mean by that:

1. **Traceability** - Give users the raw form of data. This allows them to do post-processing and aggregation on their own.
This is a first step, but if we do not ever record low-level data or details, users have no ability to do next steps manually
or contribute back to GitLab.
1. **Visibility** - Give users aggregations and reports on data. This allows them to quickly see what is going on and what
may need to be adjusted. Once they are aware of current state, they can then manually take action to resolve any issues.
1. **Actionability** - Act on our users' behalf to enforce the policies or settings they define. This is a final stage
that builds on top of traceability and visibility.

This approach is a good, iterative approach since it allows us to enable more users over time, rather than making everyone
wait until everything is completed at once. As an example, if we simply record and expose some pieces of data, that enables
users that need to implement a policy do so with some sort of custom script immediately. They may even contribute it to GitLab.
Over time, we can expand from just reporting that data to offering settings and enforcement around it. This means users that
need the capability sooner can get what they need and users we work with later get a complete offering after we have finished.

## Where we're headed

The Compliance group handles the three categories [listed above](#categories-we-focus-on) and focuses 
on advancing each of them. This means we will prioritize some before others. This section highlights and stack ranks our main 
areas of focus as a group. Please note these are the highlights and not a comprehensive list. Each specific category page has
additional details on the other epics and issues for the category we are focused on.

1. Adding new and refactoring existing audit events ([epic](https://gitlab.com/groups/gitlab-org/-/epics/736))
    * One of our users key needs is that our audit event system is comprehensive enough to record all the sensitive activities that occur in GitLab. Because GitLab releases every month, we will focus on adding new events to ensure our customers have the visibility they need.
1. Webhook-based audit events ([epic](https://gitlab.com/groups/gitlab-org/-/epics/5925))
    * Our users frequently wish to view audit events outside of GitLab, combine them with other data sources, and process them in some way.
    Rather than require users to manually download events from our API, we will deliver new audit events as they occur over a webhook. This way
    users can listen to the webhook and take any relevant action as soon as a new event occurs. This also is a benefit for GitLab, since it 
    means we do not have to store and manage the potentially millions of events that are generated so can start recording higher-volume 
    audit events that users are interested in, such as [Git activity](https://gitlab.com/gitlab-org/gitlab/-/issues/11650).
1. Updating and improving the [Compliance Dashboard](https://docs.gitlab.com/ee/user/compliance/compliance_dashboard/) ([epic](https://gitlab.com/groups/gitlab-org/-/epics/5237))
    * Compliance teams have limited bandwidth and need to be able to easily find activity that does not follow policy to either remediate or document a variance. We have a great opportunity at GitLab to update our Compliance Dashboard to be even more meaningful for this use case.
1. [Credential inventory](https://docs.gitlab.com/ee/user/admin_area/credentials_inventory.html) for GitLab.com ([epic](https://gitlab.com/groups/gitlab-org/-/epics/4994))
    * GitLab users who are self-managed get a lot of value out of our credential inventory system to understand what sort of exposure their instances are, who is using various credentials, and removing access when needed. This has previously _only_ been available to self-managed users, so we will focus on providing this same sort of experience to GitLab.com users, in line with our [SaaS first](https://about.gitlab.com/direction/#saas-first) principle.
