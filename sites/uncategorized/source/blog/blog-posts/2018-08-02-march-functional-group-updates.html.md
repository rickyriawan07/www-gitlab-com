---
title: "GitLab's Functional Group Updates - March 12th - 31st"
author: Trevor Knudsen
author_gitlab: tknudsen
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on from March 12th - 31st"
canonical_path: "/blog/2018/08/02/march-functional-group-updates/"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our GitLab Team call, a different Functional Group gives an update to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Frontend Team

[Presentation slides](https://docs.google.com/presentation/d/1M4rsLbDD3VYJYouWm_5LZZbNGbK0LswnKWUwrxuNDO4/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/quT71-XcrG0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Infrastructure Team

[Presentation slides](https://docs.google.com/presentation/d/1HZ412pPn1lW_SX7gEkx7RaJkAZhq_8OTfXBxUZkzWBA/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/84ipD7HChmg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Prometheus Team

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/1dM_pSPnIzQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

----

### Edge Team

[Presentation slides](http://gitlab-org.gitlab.io/group-conversations/edge/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/CTCZG-xquAg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/1NYr5xITRHwESuaLQ8nuRlvLEQU8BJ4BIvuafVOL6E64/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/SjHlayKuvPo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Product Team

[Presentation slides](https://www.slideshare.net/JobvanderVoort/gitlab-product-update-march-27)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Bf2en9HLW1I" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----
