---
layout: handbook-page-toc
title: "Sales Territories"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Process to Request Update

### Territory Ownership (Sales)

1. Create an issue in the **Sales Operations** project - utilizing the [Territory Change Request template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new?issuable_template=Territory_Change_Request)
1. Follow the directions within the template & provide all the requested details
    - If **Individual Contributor** is requesting the change, ADD your manager to the `/assign` command
    - If **Manager** is requesting change, submit issue & it will auto-assign to Sales Ops
1. `Sales Operations` to update SFDC
1. Change made on Territory Management document by `Sales Operations` **after** change in system has been made.
1. `Sales Operations` to update LeanData download updated csv.
1. Territory Management updates will be uploaded to LeanData by `Sales Operations` **after** change in system has been made.

### Territory Ownership (ISR)

1. Create an issue in the **Sales Operations** project - utilizing the [ISR Territory Change template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new?issuable_template=ISRTerritoryChange)
1. Follow the directions within the template & provide all the requested details
1. `Sales Operations`and `Sales Systems` to update SFDC
1. `Sales Operations`to update the [ISR Territories Mapping File](https://docs.google.com/spreadsheets/d/1dzA_t3Nfa_C-qd-QJ41BkMy6I_A76FTTmXTXb57qZuI/edit#gid=0) 
1. To request ISR ownership exceptions for individual opportunities, please chatter your ISR manager on the opportunity

#### Updating these tables without updating Operations will not be reflected in our various systems causing all reports and routing to be incorrect!

{:.no_toc}

**Questions?** Ask in `#sales` slack channel pinging `@sales-ops`.

## Region/Vertical

{:.no_toc}

**Enterprise Sales**
- **VP Enterprise Sales**: Mike Pyle
- <b>Europe, Middle East and Africa</b> (`#emea` Slack channel): TBH, Regional Director
- <b>Asia Pacific</b> (`#apac` Slack channel): Anthony McMahon, Regional Director
- <b>North America - US East</b> (`#us-east` Slack channel): Brittany Caulfield, Area Vice President
- <b>North America - US West</b> (`#us-west` Slack channel): TBH, Regional Director
- <b>Public Sector</b> (`#public-sector` Slack channel): Bob Stevens, Area Vice President
 
**Commercial Sales**
- **VP Commercial Sales** (Mid-Market & Small Business): Ryan O'Nell
- **Mid Market Global Sales**: Timmothy Ideker, Regional Director
- **SMB North America Sales** (`#smb` Slack channel): Nick Christou, Regional Director
- **SMB EMEA/APAC Sales** (`#international-smb` Slack channel): Helen Mason, Area Sales Manager

## Territories

For the detailed breakdown of how the territories are mapped please reference the [Territories Mapping File - SSoT Master tab](https://docs.google.com/spreadsheets/d/1iTDCaHN-i_xrfiv_Tkg27lYbZ3LHsERySkvv4cPsSNo/edit?usp=sharing)

### Large

#### AMER

##### Area Sales Manager

{:.no_toc}

- **NA East - Named Accounts**: TBD
- **NA East - Southeast**: TBD
- **NA East - Northeast**: Sheila Walsh
- **NA East - Central**: Adam Olson
- **NA West - Rockies/SoCal**: TBD
- **NA West - Bay Area**: Alan Cooke
- **NA West - PNW/MidWest**: Kris Nelson
- **Inside Sales - Public Sector**: Brent Caldwell
- **Inside Sales - Enterprise**: Matt Malcolm

| Sub-Region | Area | **Territory Name** | Sales |Inside Sales Rep |FMM |
| ---------- | ---- | -------------- | ----- | ----- | ----- |
|	NA East	|	East	|	Large-AMER-East-Named-1	|	Mark Bell	| David Fisher | Richard Hancock |
|	NA East	|	East	|	Large-AMER-East-Named-2	|	TBH / Adam Olson_Acting RD 	| David Fisher | Richard Hancock |
|	NA East	|	East	|	Large-AMER-East-Named-3	|	Chip Digirolamo	| David Fisher | Richard Hancock |
|	NA East	|	East	|	Large-AMER-East-Named-4	|	Josh Rector | David Fisher | Richard Hancock |
|	NA East	|	East	|	Large-AMER-East-Named-5	|	Sean Billow	| David Fisher | Richard Hancock |
|	NA East	|	East	|	Large-AMER-East-Named-6	|	TBH / Adam Olson_Acting RD	| David Fisher | Richard Hancock |
|	NA West	|	Southwest	|	Large-AMER-West-1	|	TBH / Kris Nelson	| Matthew Beadle | Rachel Hill |
|	NA West	|	Southwest	|	Large-AMER-West-2	|	TBH / Alan Cooke	| Matthew Beadle | Rachel Hill |
|	NA West	|	NorthWest	|	Large AMER-PNW/MW-1	|	Adi Wolff	| Matthew Beadle | Richard Hancock |
|	NA West	|	NorthWest	|	Large-AMER-PNW/MW-2	|	Joe Drumtra	| Matthew Beadle | Richard Hancock |
|	NA West	|	NorthWest	|	Large-AMER-PNW/MW-3	|	TBH / Kris Nelson	| Matthew Beadle | Richard Hancock |
|	NA West	|	NorthWest	|	Large-AMER-PNW/MW-4	|	Philip Wieczorek	| Matthew Beadle | Richard Hancock |
|	NA West	|	NorthWest	|	Large-AMER-PNW/MW-5	|	Lydia Pollitt	| Matthew Beadle | Richard Hancock |
|	NA West	|	NorCal	|	Large-AMER-Bay Area-1	|	Alyssa Belardi	| Matthew Beadle | Rachel Hill |
|	NA West	|	NorCal	|	Large-AMER-Bay Area-2	|	Jim McMahon	| Madison Taft | Rachel Hill |
|	NA West	|	NorCal	|	Large-AMER-Bay Area 3	|	Nico Ochoa	| Madison Taft | Rachel Hill |
|	NA West	|	NorCal	|	Large-AMER-Bay Area 4	|	Joe Miklos	| Matthew Beadle | Rachel Hill |
|	NA West	|	NorCal	|	Large-AMER-Bay Area 5	|	Michael Nevolo	| Matthew Beadle | Rachel Hill |
|	NA West	|	NorCal	|	Large-AMER-Bay Area 6	|	Michael Scott	| Madison Taft | Rachel Hill |
|	NA East	|	Northeast	|	Large-AMER-Central-1	|	Tim Kuper	| Marsja Jones | Rachel Hill |
|	NA East	|	Central	|	Large-AMER-Central-2	|	Matt Petrovick	| Marsja Jones | Rachel Hill |
|	NA East	|	Central	|	Large-AMER-Central-3	|	Brandon Greenwell	| Marsja Jones | Rachel Hill |
|	NA East	|	Central	|	Large-AMER-Central-4	|	Ruben Govender	| Marsja Jones | Rachel Hill |
|	[LATAM](/handbook/sales/territories/latam/)		|	LATAM North	|	Large-AMER-LATAM-1	|	Carlos Dominguez	| Marsja Jones | Virginia Reib |
|	[LATAM](/handbook/sales/territories/latam/)		|	LATAM South	|	Large-AMER-LATAM-2	|	Jim Torres	| Marsja Jones | Virginia Reib |
|	NA East	|	Northeast	|	Large-AMER-Northeast-1	|	TBH	\ Sheila Walsh| David Fisher | Virginia Reib |
|	NA East	|	Northeast	|	Large-AMER-Northeast-2	|	Paul Duffy	| David Fisher | Virginia Reib |
|	NA East	|	Northeast	|	Large-AMER-Northeast-3	|	Peter McCracken	| David Fisher | Virginia Reib |
|	NA East	|	Northeast	|	Large-AMER-Northeast-4	|	Tony Scafidi	| David Fisher | Virginia Reib |
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-1	|	Rick Walker	| Madison Taft | Rachel Hill |
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-2	|	Chris Cornacchia	| Madison Taft | Rachel Hill |
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-3	|	Brad Downey	| Madison Taft | Rachel Hill |
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-4	|	Steve Clark	| Madison Taft | Rachel Hill |
|	NA West	|	Southwest	|	Large-AMER-Rockies/SoCal-5	|	Robert Hyry	| Madison Taft | Rachel Hill |
|	NA East	|	Southeast	|	Large-AMER-Southeast-1	|	Chris Graham	| Marsja Jones | Virginia Reib |
|	NA East	|	Southeast	|	Large-AMER-Southeast-2	|	Katherine Evans	| Marsja Jones | Virginia Reib |
|	NA East	|	Southeast	|	Large-AMER-Southeast-3	|	Jim Bernstein	|Marsja Jones | Virginia Reib |
|	NA East	|	Southeast	|	Large-AMER-Southeast-4	|	David Wells	| Marsja Jones | Virginia Reib |
|	NA East	|	Southeast	|	Large-AMER-Southeast-5	|	Larry Biegel	| Marsja Jones | Virginia Reib |



#### Public Sector

| Sub-Region | **Territory Name** | Strategic Account Leader | Inside Sales Rep | SDR | FMM
| ---------- | -------------- | ------------------------ | ---------------- | ---------------- |---------------- |
| Public Sector | **Federal - Civilian-2** | Susannah Reed | Christine Saah | Evan Mathis |  Kira Aubrey |
| Public Sector | **Federal - Civilian-3** | Luis Vazquez | Bill Duncan | Evan Mathis | Kira Aubrey |
| Public Sector | **Federal - Civilian-5** | Joel Beck | Nathan Houston | Evan Mathis | Kira Aubrey |
| Public Sector | **Federal - Civilian-6** | Matt Kreuch | Christine Saah | Evan Mathis | Kira Aubrey |
| Public Sector | **Federal - Civilian-7** | Tyler Kensky | Christine Saah | Evan Mathis | Kira Aubrey |
| Public Sector | **State and Local (SLED East)** | Dan Samson | Alexis Shaw | Evan Mathis | Kira Aubrey |
| Public Sector | **State and Local (SLED Central)** | Matt Stamper | Victor Brew | Evan Mathis | Kira Aubrey |
| Public Sector | **State and Local (SLED South)** | Adam Mitchell | Alexis Shaw | Evan Mathis | Kira Aubrey |
| Public Sector | **Federal - DoD-Air Force-1** | Matt Jamison | Craig Pepper | Josh Downey | Helen Ortel |
| Public Sector | **Federal - DoD-Air Force-2** | TBH / Ralph Kompare | Craig Pepper | Josh Downey | Helen Ortel |
| Public Sector | **Federal - DoD-Air Force-3** | Stan Brower | Craig Pepper | Josh Downey | Helen Ortel |
| Public Sector | **Federal - DoD-Navy-1** | TBH / Ralph Kompare | Patrick Gerhold | Josh Downey | Helen Ortel |
| Public Sector | **Federal - DoD-Navy-2** | Chris Rennie | Patrick Gerhold | Josh Downey | Helen Ortel |
| Public Sector | **Federal - DoD-Army-1** | Ron Frazier | Patrick Gerhold | Josh Downey | Helen Ortel |
| Public Sector | **Federal - DoD-Army-2** | Allison Mueller | Patrick Gerhold | Josh Downey | Helen Ortel |
| Public Sector | **Federal - DoD-Agencies** | Scott McKee | Joe Fentor | Josh Downey | Helen Ortel |
| Public Sector | **Federal - NSG-1** | Marc Kriz | Joe Fenter | Josh Downey |  Helen Ortel |
| Public Sector | **Federal - NSG-2** | Mike Sellers | Joe Fenter | Josh Downey | Helen Ortel |
| Public Sector | **Federal - NSG-3** | Ian Moore | Nate Houston | Evan Mathis | Helen Ortel |
| Public Sector | **Federal - NSG-4** | Russ Wilson | Bill Duncan | Evan Mathis | Helen Ortel |
| Public Sector | **Federal - NSG-5** | Garry Judy | Nate Houston | Evan Mathis | Helen Ortel |
| Public Sector | **Federal - Channel SI** | Marty Fisher | Jay Jewell | Josh Downey | Helen Ortel |

#### APAC

| Sub-Region | Area | **Territory Name** | Sales | Inside Sales Rep | FMM |
| ---------- | ---- | -------------- | ----- | ----- | ----- |
|	Japan	|	Japan	|	Large-APAC-Japan-1	|	Tadashi Murakami	| Daphne Yam | Peter Huynh |
|	Japan	|	Japan	|	Large-APAC-Japan-2	|	Eiji Morita	| Daphne Yam | Peter Huynh |
|	Japan	|	Japan	|	Large-APAC-Japan-3	|	Yuki Murakami	| Daphne Yam | Peter Huynh |
|	APAC	|	APAC	|	Large-APAC-Large-APAC-1	|	Danny Petronio	| Daphne Yam | Peter Huynh |
|	ANZ	|	ANZ	|	Large-APAC-Large-APAC-2	|	Rob Hueston	| Daphne Yam | Peter Huynh |
|	ANZ	|	ANZ	|	Large-APAC-Large-APAC-3	|	David Haines	| Daphne Yam | Peter Huynh |
|	Asia SE	|	Southeast Asia	|	Large-APAC-Large-APAC-4	|	Hui Hui Cheong	| Daphne Yam | Peter Huynh |
|	Asia Central	|	Asia Central	|	Large-APAC-Large-APAC-5	|	Rob Hueston	| Daphne Yam | Peter Huynh |
|	Asia Central	|	Asia Central	|	Large-APAC-S Korea-1	|	Tae Ho Hyun	| Daphne Yam | Peter Huynh |

#### EMEA

| Sub-Region | Area | **Territory Name** | Sales | Inside Sales Rep | FMM |
| ---------- | ---- | -------------- | ----- | ----- | ----- |
|	Europe Central	|	Europe Central	|	Large-EMEA-DACH-1	|	Rene Hoferichter	| Anthony Seguillon | Sarina Kraft |
|	Europe Central	|	Germany	|	Large-EMEA-DACH-2	|	Thomas Schmidt Glauche	| Anthony Seguillon | Sarina Kraft |
|	Europe Central	|	Germany	|	Large-EMEA-DACH-3	|	Dirk Dornseiff	| Anthony Seguillon | Sarina Kraft |
|	Europe Central	|	Europe Central	|	Large-EMEA-DACH-4	|	Timo Schuit	| Anthony Seguillon | Sarina Kraft |
|	Europe Central	|	Germany	|	Large-EMEA-DACH-5	|	Christoph Stahl	| Anthony Seguillon | Sarina Kraft |
|	Europe Central	|	Eastern Europe	|	Large-EMEA-Large-EMEA-1	|	Vadim Rusin	| Anthony Seguillon | Sarina Kraft |
|	MEA	|	MEA	|	Large-EMEA-Large-EMEA-2	|	Phillip Smith	| Anthony Seguillon | Antonio Mimmo |
|	Southern Europe	|	Europe South	|	Large-EMEA-Large-EMEA-3	|	Anthony Seguillon (SAL)	| Anthony Seguillon | Antonio Mimmo |
|	Southern Europe	|	Europe South	|	Large-EMEA-Large-EMEA-4	|	Anthony Seguillon (SAL)	| Anthony Seguillon | Antonio Mimmo |
|	Southern Europe	|	Europe South	|	Large-EMEA-Large-EMEA-5	|	Vadim Rusin	| Anthony Seguillon | Sarina Kraft |
|	Southern Europe	|	Europe South	|	Large-EMEA-Large-EMEA-6	|	Hugh Christey	| Anthony Seguillon | Antonio Mimmo |
|	Nordics	|	Nordics	|	Large-EMEA-UK/I-1	|	Annette Kristensen	| Chris Loudon | Kristine Setschin 
|	Nordics	|	Nordics	|	Large-EMEA-UK/I-2.1	|	Aslihan Kurt	| Chris Loudon | Kristine Setschin 
|	BeNeLux	|	BeNeLux|	Large-EMEA-UK/I-2.2	|	Aslihan Kurt	| Chris Loudon | Kristine Setschin 
|	UKI	|	UKI	|	Large-EMEA-UK/I-3	|	Robbie Byrne	| Chris Loudon | Kristine Setschin |
|	UKI	|	UKI	|	Large-EMEA-UK/I-4	|	Nasser Mohunlol	| Chris Loudon | Kristine Setschin |
|	UKI	|	UKI	|	Large-EMEA-UK/I-5	|	Justin Haley	| Chris Loudon | Kristine Setschin |
|	UKI	|	UKI	|	Large-EMEA-UK/I-6	|	Nicholas Lomas	| Chris Loudon | Kristine Setschin |
|	UKI	|	UKI	|	Large-EMEA-UK/I-7	|	Warren Searle	| Chris Loudon | Kristine Setschin |
|	UKI	|	UKI	|	Large-EMEA-UK/I-8	|	Steve Challis	| Chris Loudon | Kristine Setschin |
|	UKI	|	UKI	|	Large-EMEA-UK/I-9	|	Peter Davies	| Chris Loudon | Kristine Setschin |
|	UKI	|	UKI	|	Large-EMEA-UK/I-10	|	Godwill NDulor	| Chris Loudon | Kristine Setschin |


### Mid-Market

#### AMER

| Sub-Region | Area | **Territory Name** | Sales | FMM |
| ---------- | ---- | -------------- | ----- | ----- |
|	NA East	|	US East	|	MM-AMER-East-Named-1	|	Sharif Bennett	| Virginia Reib |
|	NA East	|	US East	|	MM-AMER-East-Named-2	|	Steve Xu	| Virginia Reib |
|	NA East	|	US East	|	MM-AMER-East-Named-3	|	Daniel Parry	| Virginia Reib |
|	NA East	|	US East	|	MM-AMER-West-Named-1	|	Christopher Chiappe	| Rachel Hill | 
|	NA East	|	US East	|	MM-AMER-West-Named-2	|	Kyla Gradin	| Rachel Hill |
|	NA East	|	US East	|	MM-AMER-West-Named-3	|	Matthew Kobilka	| Rachel Hill |
|	NA East	|	US East	|	MM-AMER-EAST-CTL-1	|	Michael Miranda	| Virginia Reib |
|	NA East	|	US East	|	MM-AMER-EAST-CTL-2	|	Jenny Kline	| Virginia Reib |
|	[LATAM](/handbook/sales/territories/latam/)	|	US East	|	MM-AMER-EAST-LATAM	|	Romer Gonzalez	| Virginia Reib |
|	NA East	|	US East	|	MM-AMER-EAST-MidAtlantic	|	Jenny Kline	| Virginia Reib |
|	NA East	|	US East	|	MM-AMER-EAST-NE	|	Michael Miranda	| Virginia Reib |
|	NA East	|	US East	|	MM-AMER-EAST-SE	|	Jenny Kline	| Virginia Reib |
|	NA East	|	US East	|	MM-AMER-EAST-Southeast	|	Daniel Parry	| Virginia Reib |
|	NA West	|	US West	|	MM-AMER-WEST-Mtn	|	Adam Pestreich	| Rachel Hill |
|	NA West	|	US West	|	MM-AMER-WEST-NorCal	|	Brooke Williamson	| Rachel Hill |
|	NA West	|	US West	|	MM-AMER-WEST-NW-1	|	Adam Pestreich	| Rachel Hill |
|	NA West	|	US West	|	MM-AMER-WEST-NW-2	|	Brooke Williamson	| Rachel Hill |
|	NA West	|	US West	|	MM-AMER-WEST-SW-1	|	Adam Pestreich	| Rachel Hill |
|	NA West	|	US West	|	MM-AMER-WEST-SW-2	|	Douglas Robbin	| Rachel Hill |
|	NA West	|	US West	|	MM-AMER-WEST-SF	|	Laura Shand	| Rachel Hill |
|	NA West	|	US West	|	MM-AMER-WEST-SoCal	|	Douglas Robbin	| Rachel Hill |

#### APAC

| Sub-Region | Area | **Territory Name** | Sales | FMM |
| ---------- | ---- | -------------- | ----- | ----- | 
| ANZ	|	ANZ	|	MM-APAC-ANZ-1	|	Jo-Ming Huang	| Peter Huynh |
| ANZ	|	ANZ	|	MM-APAC-ANZ-2	|	Jo-Ming Huang	| Peter Huynh |
|	Asia Central	|	Asia Central	|	MM-APAC-Central Asia	|	Ishan Padgotra	| Peter Huynh |
|	Japan	|	Japan	|	MM-APAC-Japan	|	Ian Chiang	| Peter Huynh |
|	Asia Central	|	Pakistan	|	MM-APAC-Pakistan	|	Ishan Padgotra	| Peter Huynh |
|	Asia SE	|	Southeast Asia	|	MM-APAC-SE Asia	|	Ian Chiang	| Peter Huynh |
|	Asia South	|	Asia South	|	MM-APAC-South Asia	|	Ishan Padgotra	| Peter Huynh |

#### EMEA

| Sub-Region | Area | **Territory Name** | Sales | ISR | FMM |
| ---------- | ---- | -------------- | ----- | ----- | ----- |
|	EMEA	|	MEA	|	MM-EMEA-Named-1	|	Anthony Ogunbowale-Thomas | Shay Fleming | Kristine Setschin |
|	EMEA	|	MEA	|	MM-EMEA-Named-2	|	Israa Mahros	| Shay Fleming | Kristine Setschin 
|	Africas	|	Africas	|	MM-EMEA-Africas	|	Daisy Miclat	| Shay Fleming | Kristine Setschin |
|	Benelux	|	BE/LU	|	MM-EMEA-Benelux-BeLu	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Benelux	|	NL	|	MM-EMEA-Benelux-NL 10x-19x	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Benelux	|	NL	|	MM-EMEA-Benelux-NL 20x-29x	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Benelux	|	NL	|	MM-EMEA-Benelux-NL 30x-39x	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Benelux	|	NL	|	MM-EMEA-Benelux-NL 40x-99x	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	AT	|	MM-EMEA-Central-AT	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	CH	|	MM-EMEA-Central-CH	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 0x	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 1x	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 2x	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 3x	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 4x	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 5x	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 6x	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 7x	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 8x	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	DE	|	MM-EMEA-Central-DE 9x	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Europe Central	|	LI	|	MM-EMEA-Central-LI	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	Europe CEE	|	R	|	MM-EMEA-Eastern Europe	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	France	|	FR	|	MM-EMEA-France-FR 0x-6x	|	Israa Mahros	| Shay Fleming | Kristine Setschin 
|	France	|	FR	|	MM-EMEA-France-FR 7x	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	France	|	FR	|	MM-EMEA-France-FR 8x-9x	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	France	|	R	|	MM-EMEA-France-R	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	MEA	|	AE	|	MM-EMEA-MEA-AE	|	Daisy Miclat	| Shay Fleming | Kristine Setschin |
|	MEA	|	R	|	MM-EMEA-MEA-R	|	Daisy Miclat	| Shay Fleming | Kristine Setschin |
|	MEA	|	SA	|	MM-EMEA-MEA-SA	|	Daisy Miclat	| Shay Fleming | Kristine Setschin |
|	Nordics	|	DK	|	MM-EMEA-Nordics-DK	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Nordics	|	FI	|	MM-EMEA-Nordics-FI	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	Nordics	|	NO	|	MM-EMEA-Nordics-NO	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Nordics	|	R	|	MM-EMEA-Nordics-R	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Nordics	|	SE	|	MM-EMEA-Nordics-SE	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	Russia	|	RU	|	MM-EMEA-Russia	|	Daisy Miclat	| Shay Fleming | Kristine Setschin |
|	Southern Europe	|	ES	|	MM-EMEA-Southern-ES	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Southern Europe	|	IL	|	MM-EMEA-Southern-IL	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	Southern Europe	|	IT	|	MM-EMEA-Southern-IT	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Southern Europe	|	PT	|	MM-EMEA-Southern-PT	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	Southern Europe	|	R	|	MM-EMEA-Southern-R	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	UKI	|	GB	|	MM-EMEA-UKI-GB	|	Daisy Miclat	| Shay Fleming | Kristine Setschin |
|	UKI	|	IE	|	MM-EMEA-UKI-Ireland	|	Hans Frederiks	| Shay Fleming | Kristine Setschin |
|	UKI	|	GB	|	MM-EMEA-UKI-London E	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	UKI	|	GB	|	MM-EMEA-UKI-London EC	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	UKI	|	GB	|	MM-EMEA-UKI-London N	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	UKI	|	GB	|	MM-EMEA-UKI-London NW	|	Chris Willis	| Shay Fleming | Kristine Setschin |
|	UKI	|	GB	|	MM-EMEA-UKI-London SE	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	UKI	|	GB	|	MM-EMEA-UKI-London SW	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	UKI	|	GB	|	MM-EMEA-UKI-London W	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	UKI	|	GB	|	MM-EMEA-UKI-London WC	|	Conor Brady	| Shay Fleming | Kristine Setschin |
|	UKI	|	R	|	MM-EMEA-UKI-R	|	Daisy Miclat	| Shay Fleming | Kristine Setschin |

### Mid-Market First Order

| Sub-Region | Area | **Territory Name** | Sales | FMM |
| ---------- | ---- | -------------- | ----- | ----- | 
|	NA East	|	US East	|	MM-AMER-FO-East	|	Todd Lauver	| Rachel Hill |
|	NA West	|	US West	|	MM-AMER-FO-West	|	Todd Lauver	| Rachel Hill |
|	NA Central	|	US Central	|	MM-AMER-FO-Central	|	Lexi Roman	| Rachel Hill |
|	Northern Europe	|	Northern Europe	|	MM-EMEA-FO-North	|	Lisa VdKooij	|  Kristine Setschin |
|	Southern Europe	|	Southern Europe	|	MM-EMEA-FO-South	|	Sophia Simunec	| Kristine Setschin |

### SMB

#### AMER

| Sub-Region | Area | **Territory Name** | Sales |
| ---------- | ---- | -------------- | ----- | 
|	AMER	|	AMER	|	SMB-AMER-EAST-CTL-1	|	Jenny Chapman
|	AMER	|	AMER	|	SMB-AMER-EAST-CTL-2	|	Anthony Feldman
|	AMER	|	AMER	|	SMB-AMER-EAST-CTL-3	|	Matthew Walsh
|	AMER	|	AMER	|	SMB-AMER-EAST-CTL-4	|	Kaley Johnson
|	[LATAM](/handbook/sales/territories/latam/)	|	US East	|	SMB-AMER-EAST-LATAM	|	Romer Gonzalez
|	AMER	|	AMER	|	SMB-AMER-EAST-MidAtl	|	Jenny Chapman
|	AMER	|	AMER	|	SMB-AMER-EAST-NE	|	Matthew Walsh
|	AMER	|	AMER	|	SMB-AMER-EAST-NY	|	Anthony Feldman
|	AMER	|	AMER	|	SMB-AMER-EAST-SE	|	Kaley Johnson
|	AMER	|	AMER	|	SMB-AMER-WEST-NorCal |	James Altheide
|	AMER	|	AMER	|	SMB-AMER-WEST-NW	|	Blake Chalfant-Kero
|	AMER	|	AMER	|	SMB-AMER-WEST-SF/LA	|	TBH - James Komara
|	AMER	|	AMER	|	SMB-AMER-WEST-SoCal	|	Jake Grothjan
|	AMER	|	AMER	|	SMB-AMER-WEST-SW	|	Carrie Nicholson

#### APAC

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- |
|	ANZ	|	ANZ	|	SMB-APAC-ANZ	|	Ishan Padgotra	|
|	APAC	|	APAC	|	SMB-APAC-JAPAN	|	Ishan Padgotra	|
|	APAC	|	APAC	|	SMB-APAC-SE Asia	|	Ishan Padgotra	|
|	APAC	|	APAC	|	SMB-APAC-Central Asia	|	Ishan Padgotra	|
|	APAC	|	APAC	|	SMB-APAC-South Asia	|	Ishan Padgotra	|

#### EMEA

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- | --- |
|	Europe Central	|	BeNeLux	|	SMB-EMEA-BeNeLux	|	Vilius Kavaliauskas
|	EMEA	|	EMEA	|	SMB-EMEA-DE-1	|	Gábor Zaparkanszky
|	EMEA	|	EMEA	|	SMB-EMEA-DE-2	|	Veronika Herzberger
|	EMEA	|	EMEA	|	SMB-EMEA-DE-3	|	Rahim Abdullayev
|	Europe CEE	|	Eastern Europe	|	SMB-EMEA-Eastern Europe	|	Arina Voytenko
|	Southern Europe	|	EMEA	|	SMB-EMEA-FR-1	|	Wiam Aissaoui
|	Southern Europe	|	EMEA	|	SMB-EMEA-FR-2	|	Wiam Aissaoui
|	Southern Europe	|	Europe South	|	SMB-EMEA-Greece	|	Arina Voytenko
|	MEA	|	MEA	|	SMB-EMEA-MEA	|	Camilo Villanueva
|	Nordics	|	Nordics	|	SMB-EMEA-Nordics-1	|	Goran Bijelic
|	Nordics	|	Nordics	|	SMB-EMEA-Nordics-2	|	Goran Bijelic
|	Europe Central	|	DACH	|	SMB-EMEA-Rest of DACH	|	Rahim Abdullayev
|	Southern Europe	|	Europe South	|	SMB-EMEA-Southern Europe	|	Camilo Villanueva
|	UKI	|	UKI	|	SMB-EMEA-UKI-1	|	Camilo Villanueva
|	UKI	|	UKI	|	SMB-EMEA-UKI-2	|	Noria Aidam

### SMB First Order

| Sub-Region | Area | **Territory Name** | Sales | 
| ---------- | ---- | -------------- | ----- | 
|	AMER	|	US West	|	SMB-AMER-West-FO	|	Da'Neil Olsen	|
