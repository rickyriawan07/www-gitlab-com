---
layout: handbook-page-toc
title: "Field Operations Changelog"
description: "A running log of Field Operations changes (Sales Ops, CS Ops, Channel Ops & Deal Desk) organized by quarter and further by bi-weekly releases."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Field Operations Changelog keeps a running log of Field Ops changes related to Sales Ops, CS Ops, Channel Ops & Deal Desk. The log is organized by fiscal year/quarter and sub-organized by the bi-weekly Field Ops release milestone/epic. It was started in Q3-FY22 and is organized by newest updates at the top.

To learn more about Field Ops releases, see the [Field Ops Release Schedule handbook page] (link TBD). 

## Q3-FY22 

### 2021-08-16 Release ([Epic] (link TBD))
Item 1 (title only) (issue link)

Item 2 (issue link)

Item 3 (issue link)

Item 4 (issue link)

Item 4 (issue link)

### 2021-08-02 Release
[Partner Program and System Changes](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/49)

[FY22 Billing and Subscription Management Experience Improvements (Project Super Sonics) - Sales Order Processing Handbook Page Updates](/handbook/sales/field-operations/order-processing/#supersonics-billing-and-subscription-management-experience)

[Deal Desk Smart Templates](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/32)

[Expanded Criteria for Required Contact Role "GitLab Admin" as part of CLOSED WON for Enterprise/Commercial](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/1831#note_637952055)

[Sales Ops FY22 Q2 Account Moves](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/2347)


